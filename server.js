global.appRoot = __dirname;
const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const port = 3000
const conn = require(appRoot + "/server/dbConnection").conn;
const mysql = require('mysql');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.get('/api/products', (req, res) => {
  var q = "SELECT Product.ID as id, Product.Name as product_name, Product.Description as description, Product.Image as img, Product.Quantity as quantity, Price.Currency as currency, Price.Amount as amount FROM Product LEFT JOIN Price ON Product.ID=Price.ProductID";
  conn.query(q, function (error, results) {
  if (error) throw error;
  console.log('The solution is: ', results);
  res.status(200).json({response : results})
  });
})

app.post('/api/products', (req, res) => {
  console.log(req.body);
  const post = {
    Name: req.body.name,
    Description: req.body.description,
    Image: req.body.image,
    Quantity: req.body.quantity,
  };
  const q = "INSERT INTO Product SET ?";
  conn.query(q, post, function (error, results, fields) {
  if (error) throw error;
    console.log(results.insertId);
    const price = {
      ProductID: results.insertId,
      Currency: req.body.currency,
      Amount: req.body.amount
    }
    const q = "INSERT INTO Price SET ?";
    conn.query(q, price, function (error, results, fields) {
      if (error) throw error;
      res.status(200).json({response : 'Product Added'});
    });
  });
});

app.put('/api/products/:id', (req, res) => {
  console.log(req.body);
  const post = [
    req.body.name,
    req.body.description,
    req.body.image,
    req.body.quantity,
    parseInt(req.params.id)

  ];
  console.log(post);
  const q = "UPDATE Product SET Name = ?, Description = ?, Image = ?, Quantity = ? WHERE ID = ?";
  console.log(mysql.format(q, post));
  conn.query(q, post, function (error, results, fields) {
  if (error) throw error;
  const price = [
    req.body.currency,
    req.body.amount,
    parseInt(req.params.id),
  ]
  const q = "UPDATE Price SET Currency = ?, Amount = ? WHERE ProductID = ?";
    console.log(mysql.format(q, price));
    conn.query(q, price, function (error, results, fields) {
      if (error) throw error;
      res.status(200).json({response : 'Product Updated'});
    });
  });
});



app.delete('/api/products/:id', (req, res) => {
  const q = "DELETE FROM Price WHERE ProductID = ?";
  console.log(mysql.format(q, [req.params.id]));
  conn.query(q, req.params.id, function (error, results, fields) {
    if (error) throw error;
    const q = "DELETE FROM Product WHERE ID = ?";
    console.log(mysql.format(q, [req.params.id]));
    conn.query(q,[req.params.id], function (error, results, fields) {
    if (error) throw error;
    res.status(200).json({response : 'Product Deleted'});
    });
  });
});




app.listen(port, () => console.log(`Example app listening on port ${port}!`))
