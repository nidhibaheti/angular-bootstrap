var mysql = require('mysql');
var conn  = mysql.createPool({
  connectionLimit : 10,
  host            : 'localhost',
  port            : '8889',
  user            : 'nidhi',
  password        : 'nidhi',
  database        : 'angularapp'
});

module.exports = {conn: conn};
