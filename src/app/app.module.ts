import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule }    from '@angular/common/http';


import { NgModule } from '@angular/core';

import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule, MatCardModule, MatFormFieldModule} from "@angular/material";
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductComponent } from './product/product.component';
import {HttpErrorHandler} from "./http-error-handler.service";
import {ProductService} from "./services/product.service";
import { AddProductComponent } from './add-product/add-product.component';
import {ReactiveFormsModule} from "@angular/forms";
import {EditProductDialogComponent} from "./product/dialogs/edit-product/edit-product-dialog.component";
import {DeleteProductDialogComponent} from "./product/dialogs/delete-product/delete-product-dialog.component";


@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    AddProductComponent,
    EditProductDialogComponent,
    DeleteProductDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatFormFieldModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [HttpErrorHandler, ProductService],
  entryComponents: [EditProductDialogComponent, DeleteProductDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
