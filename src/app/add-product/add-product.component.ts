import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import {ProductService} from "../services/product.service";
import {Router} from "@angular/router";


@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {
  form;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private productService: ProductService) { }

  ngOnInit() {
    this.form = this.fb.group({
      name: [null, Validators.required],
      description: [null, Validators.required],
      image: [null, Validators.required],
      quantity: [1, Validators.required],
      amount: [null, Validators.required],
      currency: ['USD', Validators.required],
    });

  }

  addProduct() {
    const {name, description, image, quantity, amount, currency} = this.form.getRawValue();
    const requestBody = {
      name,
      description,
      image,
      quantity,
      amount,
      currency
    };
    this.productService.addProduct(requestBody).subscribe(data => {
        this.router.navigate(['/products']);

    },
    error => console.error(error));
  }
}
