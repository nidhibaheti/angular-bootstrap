import { Component, OnInit } from '@angular/core';
import {ProductService} from "../services/product.service";
import {MatDialog} from "@angular/material";
import {EditProductDialogComponent} from "./dialogs/edit-product/edit-product-dialog.component";
import {DeleteProductDialogComponent} from "./dialogs/delete-product/delete-product-dialog.component";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  products;

  constructor(private productService : ProductService, public dialog: MatDialog) { }

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.productService.getProducts().subscribe(
      data => {
        this.products = data.response;
      },
      error => console.log(error)
    );
  }

  editProductClicked(product) {
    this.dialog.open(EditProductDialogComponent, {
      width: '400px',
      data: {
        product: product
      }
    }).afterClosed().subscribe(res => {
      console.log(res);
      if (res.status === 'success') {
        this.getProducts();
      }
    });
  }

  deleteProductClicked(product) {
    this.dialog.open(DeleteProductDialogComponent, {
      data: {
        product: product
      }
    }).afterClosed().subscribe(res => {
      if (res.status === 'success') {
        this.products = this.products.filter(p => p.id !== product.id);
      }
    });
  }
}
