import {Component, Inject, OnInit} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material";
import {FormBuilder, Validators} from "@angular/forms";
import {ProductService} from "../../../services/product.service";
@Component({
  selector: 'app-edit-product-dialog',
  templateUrl: './edit-product-dialog.component.html',
  styleUrls: ['./edit-product-dialog.component.scss']
})
export class EditProductDialogComponent implements OnInit{
  form;
  constructor(@Inject(MAT_DIALOG_DATA) public data,
              private productService: ProductService,
              private dialogRef: MatDialogRef<EditProductDialogComponent>,
              private fb: FormBuilder,
  ) {
    console.log(this.data);
  }

  ngOnInit() {
    const {product_name, description, img, quantity, amount, currency} = this.data.product
    this.form = this.fb.group({
      name: [product_name, Validators.required],
      description: [description, Validators.required],
      image: [img, Validators.required],
      quantity: [quantity, Validators.required],
      amount: [amount, Validators.required],
      currency: [currency, Validators.required],
    });
  }

  editProduct() {
    const {name, description, image, quantity, amount, currency} = this.form.getRawValue();
    const requestBody = {
      name,
      description,
      image,
      quantity,
      amount,
      currency
    };

    this.productService.editProduct(requestBody, this.data.product.id).subscribe(data => {
      this.dialogRef.close({status: 'success'});
    }, error => this.dialogRef.close());


  }
}
