import {Component, Inject} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {ProductService} from "../../../services/product.service";
@Component({
  selector: 'app-delete-product-dialog',
  templateUrl: './delete-product-dialog.component.html',
})
export class DeleteProductDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data,
              private dialofRef: MatDialogRef<DeleteProductDialogComponent>,
              private productService: ProductService) {}

  deleteProductClicked() {
    this.productService.deleteProduct(this.data.product.id).subscribe(
      data => this.dialofRef.close({status: 'success'}),
      error => this.dialofRef.close()
    );
  }
}
