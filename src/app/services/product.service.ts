import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';


import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandler, HandleError } from '../http-error-handler.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

@Injectable()
export class ProductService {
  handleError
  serverUrl = '/api/products';  // URL to web api

  constructor(
    private http: HttpClient,
    private httpErrorHandler: HttpErrorHandler
    ) {
    this.handleError = httpErrorHandler.createHandleError('ProductService');
  }

  /** GET products from the server */
  getProducts() {
    return this.http.get(this.serverUrl)
      .pipe(
        catchError(this.handleError('getProducts', []))
      );
  }

  addProduct(product) {
    return this.http.post(this.serverUrl, product, httpOptions)
      .pipe(
        catchError(this.handleError('addProduct', []))
      );
  }

  editProduct(product, id) {
    return this.http.put(`${this.serverUrl}/${id}`, product, httpOptions)
      .pipe(
        catchError(this.handleError('editProduct', []))
      );
  }

  deleteProduct(id) {
    return this.http.delete(`${this.serverUrl}/${id}`, httpOptions)
      .pipe(
        catchError(this.handleError('deleteProduct', []))
      );
  }

}

